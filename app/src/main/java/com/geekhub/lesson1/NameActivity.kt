package com.geekhub.lesson1

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText



class NameActivity : AppCompatActivity() {


    private var editText: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_name)
        editText = findViewById(R.id.editText);
    }

    fun onClick(view: View){
        setResult(Activity.RESULT_OK, Intent().putExtra("name", editText?.text.toString()))
        finish()
    }
}
