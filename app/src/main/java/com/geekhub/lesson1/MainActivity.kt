package com.geekhub.lesson1

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast




class MainActivity : AppCompatActivity() {

    private var textView: TextView? = null
    private var button: Button? = null
    private var linearLayout: LinearLayout? = null
    private var imageView: ImageView? = null
    public final val CODE = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView = findViewById(R.id.textView)
        button = findViewById(R.id.button)

        linearLayout = findViewById(R.id.linearLayout);
    }

    fun onClick(view: View){
        val intent = Intent(this, NameActivity::class.java)
        startActivityForResult(intent, CODE)
    }

    fun feedback(view: View){
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("ZheleznyiLord@gmail.com"))
        intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
        intent.putExtra(Intent.EXTRA_TEXT, "Please, input your feedback")
        intent.setPackage("com.google.android.gm")
        if (intent.resolveActivity(packageManager) != null)
            startActivity(intent)
        else
            Toast.makeText(this, "Gmail App is not installed", Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data?.getStringExtra("name")
                textView?.text = result
                button!!.text = getString(R.string.ChangeButton)
                imageView = ImageView(this)
                var res = choosePicture(result)
                imageView?.setImageResource(res)
                linearLayout?.addView(imageView)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        linearLayout?.removeView(imageView)
    }

    fun choosePicture(result: String?): Int {
        when(result?.toLowerCase()){
            "vadim"-> return R.drawable.vadim
            "cheb"-> return R.drawable.serhii
            "чеб"-> return R.drawable.serhii
            "сергей"-> return R.drawable.serhii
            "sergey"-> return R.drawable.serhii
            "ruslan"-> return R.drawable.ruslan
            "руслан"-> return R.drawable.ruslan
            "vlad"-> return R.drawable.vlad
            "vladislav"-> return R.drawable.vlad
            "влад"-> return R.drawable.vlad
            "владислав"-> return R.drawable.vlad
            "alex"-> return R.drawable.alex
            "aleksandr"-> return R.drawable.alex
            "oleksandr"-> return R.drawable.alex
            "sasha"-> return R.drawable.alex
            "олександр"-> return R.drawable.alex
            "александр"-> return R.drawable.alex
            "саша"-> return R.drawable.alex
            "Андрей"-> return R.drawable.andrii
            "Андрій"-> return R.drawable.andrii
            "Andriy"-> return R.drawable.andrii
            "Andrey"-> return R.drawable.andrii
            else -> return R.drawable.quest
        }
    }

}
